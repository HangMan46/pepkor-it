﻿select u.* from tUSER u where
   u.id in (select p.tUSER_id from tPROFILE p where
      p.tTYPES_id in (
         select t.id from tTYPES t where
            t.description = 'Cellphone' and t.deleted = 0
      ) and p.value = $MSISDN
   ) and u.id_number = $ID_NUMBER
