﻿
CREATE TABLE TYPES ( --OPERATIONS_USER/DRIVER_USER/VIN_SD/ID_BOOK_SD
id INTEGER NOT NULL PRIMARY KEY, --NO AUTO_INCREMENT
name VARCHAR(100) NOT NULL,
description VARCHAR(100) NOT NULL
);

CREATE TABLE SUPPORTING_DOCUMENT ( --VIN_SD/ID_BOOK_SD
id INTEGER NOT NULL PRIMARY KEY, --NO AUTO_INCREMENT
name VARCHAR(100) NOT NULL,
value VARCHAR(100) NOT NULL,
type_id INTEGER NOT NULL REFERENCES TYPES
);

CREATE TABLE VEHICLES (
id INTEGER NOT NULL PRIMARY KEY, --NO AUTO_INCREMENT
mileage INTEGER NOT NULL,
supporting_document_id INTEGER NOT NULL REFERENCES SUPPORTING_DOCUMENT --VIN_SD
);

CREATE TABLE USERS ( --OPERATIONS_USER/DRIVER_USER
id INTEGER NOT NULL PRIMARY KEY, --NO AUTO_INCREMENT
first_names VARCHAR(100) NOT NULL,
last_name VARCHAR(100) NOT NULL,
supporting_document_id INTEGER NOT NULL REFERENCES SUPPORTING_DOCUMENT, --ID_BOOK_SD
type_id INTEGER NOT NULL REFERENCES TYPES
);

CREATE TABLE ROUTES (
id INTEGER NOT NULL PRIMARY KEY, --NO AUTO_INCREMENT
start_address VARCHAR(100) NOT NULL,
end_address VARCHAR(100) NOT NULL
);

CREATE TABLE DRIVERS_SCHEDULES (
id INTEGER NOT NULL PRIMARY KEY, --NO AUTO_INCREMENT
vehicles_id INTEGER NOT NULL REFERENCES VEHICLES,
users_id INTEGER NOT NULL REFERENCES USERS,
routes_id INTEGER NOT NULL REFERENCES ROUTES,
schedule_datetime DATE NOT NULL,
start_date DATE NOT NULL,
end_date DATE NOT NULL
);
