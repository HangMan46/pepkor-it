if test -z $DB_NAME;then
   echo "No DB_NAME specified, please set this as an environmental variable before next attempt"
   exit 1
fi
psql -d $DB_NAME -f dropSchema.ddl
