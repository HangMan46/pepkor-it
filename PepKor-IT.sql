﻿SELECT 
  * 
FROM 
  public.drivers_schedules, 
  public.vehicles, 
  public.users, 
  public.routes
WHERE 
  drivers_schedules.vehicles_id = vehicles.id AND
  drivers_schedules.users_id = users.id AND
  drivers_schedules.routes_id = routes.id;
